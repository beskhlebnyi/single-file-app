# Requirements: 
require 'sinatra'
require 'sinatra/reloader' if development?
require 'slim'

# Database initialization:
require 'sequel'
# Fill with your pg user data
# DB = Sequel.postgres 'postgres', user:'pg_user_name', password:'pg_user_password', host:'localhost'

# Or create user with same name and password:
DB = Sequel.postgres 'postgres',
                     user: ENV['PG_USER'], 
                     password: ENV['PG_PASSWORD'], 
                     host:'localhost'

DB << "SET CLIENT_ENCODING TO 'UTF8';"

DB.create_table? :expenses do
  primary_key :id
  String :name
  Integer :quantity
end

# Expense model:
class Expense < Sequel::Model
end


# Home routes:
get '/' do
  slim :index
end

get '/about-us' do
  slim :about_us
end

helpers do
  def calc(first, second)
    first + second
  end
end


# Expenses routes:
get '/expenses' do
  expenses = Expense.all
  slim :'expenses/index', locals: { expenses: expenses }
end

get '/expenses/create' do
  slim :'expenses/create'
end

post '/expenses' do
  expense = Expense.new
  expense.name = params[:name]
  expense.quantity = params[:quantity]
  expense.save
  redirect '/expenses'
end

put '/expenses/:id' do
  expense = Expense[params[:id]]
  expense.name = params[:name]
  expense.quantity = params[:quantity]
  expense.save
  redirect '/expenses'
end

delete '/expenses/:id' do
  Expense[params[:id]].destroy
  redirect '/expenses'
end

get '/expenses/:id' do
  expense = Expense[params[:id]]
  slim :'expenses/show', locals: { expense: expense }
end

get '/expenses/:id/edit' do
  expense = Expense[params[:id]]
  slim :'expenses/edit', locals: { expense: expense }
end

__END__

# Layout:
@@ layout
doctype html
html
  head
    title = 'Smart TODO'
    link(rel="icon" type="image/png" href="/favicon.png")
    meta(http-equiv="X-UA-Compatible" content="IE=8")
    meta(http-equiv="Content-Script-Type" content="text/javascript" )
    meta(http-equiv="Content-Style-Type" content="text/css" )
    meta(http-equiv="Content-Type" content="text/html; charset=utf-8" )
    meta(http-equiv="expires" content="0" )
    meta(name="Ivan Zaytsev, Yaroslav Beskhlebnyi" content="Single-file todo app.")
  body
    == yield
  
  sass:
    body 
      margin: 0 auto
      text-align: center
    
    .main-title 
      color: blue

    .expense-form
      display: center
      margin: 0 auto
    

  javascript:
    console.log("JS is working")

@@ index
div
  h1.main-title  Hello world
  a href='/expenses'  Expenses page
  p = calc(5,1)
  a href='/about-us'  About us

@@ about_us
div.about-us
  p Trying to create single-file app.
  a href='/'  Back to main page


@@ expenses/index
.expenses
  h1 Your expenses:
  a href="/expenses/create"  Create new expense
  hr
  table.expenses-table
    - expenses.each do |expense|
      tr  
        td
          a href="expenses/#{expense.id}" = expense.name
        td 
          = expense.quantity
        td
          form action='/expenses/#{expense.id}' method='POST' class='inline' 
            input type='hidden' name='_method' value='DELETE'
            input type='submit' value='Delete'
      
hr
a href='/'  Back to main page

sass:
  .expenses-table
    display: center
    margin: 0 auto

@@ expenses/create
form action="/expenses" method='POST'
  table.create-form
    tr
      td
        label Name:
      td
        input type='text' name='name'
    tr
      td 
        label Quantity:
      td 
        input type='text' name='quantity'
  br
  input type='submit' value='Create'

sass:
  .create-form
    display: center
    margin: 0 auto

@@ expenses/show 
- if expense  
  h1 Expense
  hr
  table.expense-form
    tr
      td 
        = expense.name
      td 
        = expense.quantity
      td
        a href="/expenses/#{expense.id}/edit"  Edit
  hr
  a href='/expenses'  Back to expenses page
- else
  p 
    | Expense does not exist
  hr
  a href='/expenses'  Back to expenses page

@@ expenses/edit
form action="/expenses/#{expense.id}" method='POST'
  table.expense-form
    tr
      td
        label Name:
      td
        input type='text' name='name' value="#{expense.name}"
    tr
      td 
        label Quantity:
      td 
        input type='text' name='quantity' value="#{expense.quantity}"
  br
  input type='hidden' name='_method' value='PUT'
  input type='submit' value='Edit'

br
a href='/expenses'  Back to expenses page
